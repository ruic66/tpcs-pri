<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">
    
    <xsl:output method="html" indent="yes" encoding="UTF-8"/>
    
    <xsl:template match="/">
        <html>
            <head>
                <title>Manifesto</title>
                <meta charset="UTF-8" />
                <link href="https://fonts.googleapis.com/css?family=Amiri" rel="stylesheet" />
                <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css" />
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
            </head>
            <body style="font-family: 'Amiri';">
                <xsl:apply-templates/>
                <table class="w3-display-bottomright" style="position: fixed;">
                    <tr>
                        <td>
                            <button onclick="showAllContainers()" class="w3-button w3-white w3-hover-white w3-block">
                                Abrir Todos</button>
                        </td>
                        <td >
                            <button onclick="hideAllContainers()" class="w3-button w3-white w3-hover-white w3-block">
                                Fechar Todos</button>
                        </td>
                    </tr>
                </table>
                <script>
                    
                    function toggleContainer(id) {
                    var x = document.getElementById(id);
                    if (x.className.indexOf("w3-show") == -1) {
                    x.className += " w3-show";
                    } else { 
                    x.className = x.className.replace(" w3-show", "");
                    }
                    }
                    
                    function showContainer(id) {
                    var x = document.getElementById(id);
                    if (x.className.indexOf("w3-show") == -1) {
                    x.className += " w3-show";
                    }
                    }
                    
                    function hideContainer(id) {
                    var x = document.getElementById(id);
                    if (x.className.indexOf("w3-show") != -1) {
                    x.className = x.className.replace(" w3-show", "");
                    }
                    }
                    
                    function showAllContainers() {
                        showContainer('Informacoes');
                        showContainer('Equipa');
                        showContainer('Resumo');
                        showContainer('Resultados');
                    }
                    
                    function hideAllContainers() {
                    hideContainer('Informacoes');
                    hideContainer('Equipa');
                    hideContainer('Resumo');
                    hideContainer('Resultados');
                    }
                </script>
            </body>

        </html>
    </xsl:template >
    
    <xsl:template match="meta">
        <div class="w3-container w3-center w3-border">
            <h1 style="font-family: 'Amiri';"><xsl:value-of select="titulo"/></h1>
            <h2 style="font-family: 'Amiri';"><xsl:value-of select="subtitulo"/></h2>
        </div>
        <button onclick="toggleContainer('Informacoes')" 
                class="w3-button w3-white w3-hover-white w3-block">
            <i class="fa fa-info-circle"></i><b> Informações Gerais</b>
        </button>
        <div id='Informacoes' class="w3-container w3-border w3-hide w3-show">
            <table class="w3-table">
                <tr>
                    <td>
                        <b>Título: </b><xsl:value-of select="titulo"/>
                        <br/>
                        <b>Subtítulo: </b><xsl:value-of select="subtitulo"/>
                        <br/>
                        <b>ID: </b><xsl:value-of select="id"/>
                    </td>
                    <td>
                        <xsl:apply-templates/>
                        <br/>
                        <b>Início: </b> <xsl:value-of select="dinicio"/>
                        <br/>
                        <b>Fim: </b> <xsl:value-of select="dfim"/>
                    </td>
                </tr>
            </table>
            
        </div>
    </xsl:template>
    
    <xsl:template match="titulo"></xsl:template>
    <xsl:template match="subtitulo"></xsl:template>
    <xsl:template match="id"></xsl:template>
    <xsl:template match="dinicio"></xsl:template>
    <xsl:template match="dfim"></xsl:template>
    
    <xsl:template match="supervisor">
        <b>Supervisor: </b>
        <xsl:value-of select="nome"/>
        <span style="margin-right: 10px;"></span>
        <xsl:choose>        
        <xsl:when test="website">
        <a href="{website}" target="_blank">
            <i class="fa fa-globe"></i> 
        </a>
        <span style="margin-right: 10px;"></span>
        </xsl:when>
        
        </xsl:choose>
        <a href="mailto:{email}">
            <i class="fa fa-envelope"></i> 
        </a>
    </xsl:template>
    
    <xsl:template match="equipa">
        <button onclick="toggleContainer('Equipa')" class="w3-button w3-white w3-hover-white w3-block">
            <i class="fa fa-users"></i><b> Equipa</b></button>
        <div id='Equipa' class="w3-container w3-border w3-bar w3-hide">
        <ul>
            <xsl:apply-templates/>
        </ul>
        </div>
    </xsl:template>
    
    <xsl:template match="elemento">
        <li class="w3-bar-item">
            <xsl:choose>
                <xsl:when test="foto">
                    <img src="{foto/@path}" class="w3-bar-item w3-roudn" style="width:120px; height: 120px;" />   
                </xsl:when>
                <xsl:otherwise>
                    <img src="no_img.gif" class="w3-bar-item w3-round" style="width:120px; height: 120px;" />
                </xsl:otherwise>
            </xsl:choose>

            <p class="w3-bar-item">
                <xsl:value-of select="nome"/> <br/>
                <xsl:value-of select="id"/>
                <span style="margin-right: 10px;"></span>
                <xsl:choose>
                    <xsl:when test="website">
                        <a href="{website}">
                            <i class="fa fa-globe"></i> 
                        </a>
                        <span style="margin-right: 10px;"></span>
                    </xsl:when>
                </xsl:choose>
                <a href="mailto:{email}">
                    <i class="fa fa-envelope"></i> 
                </a>
            </p>
        </li>
    </xsl:template>
    
    <xsl:template match="resumo">
        <button onclick="toggleContainer('Resumo')" class="w3-button w3-white w3-hover-white w3-block">
            <i class="fa fa-align-left"></i><b> Resumo</b></button>
        <div id='Resumo' class="w3-container w3-border w3-hide">
        <xsl:apply-templates/>
        </div>
    </xsl:template>
    
    <xsl:template match="para">
        <p><xsl:apply-templates/></p>
    </xsl:template>
    
    <xsl:template match="b">
        <b><xsl:apply-templates/></b>
    </xsl:template>
    
    <xsl:template match="i">
        <i><xsl:apply-templates/></i>
    </xsl:template>
    
    <xsl:template match="resultados">
        <button onclick="toggleContainer('Resultados')" class="w3-button w3-white w3-hover-white w3-block">
            <i class="fa fa-file"></i><b> Resultados</b></button>
        <div id='Resultados' class="w3-container w3-border w3-hide">
        <ol class="w3-ol">
            <xsl:apply-templates/>
        </ol>
        </div>
    </xsl:template>
    
    <xsl:template match="resultado">
        <li class="w3-bar w3-block">
            <span class="w3-bar-item">
                <xsl:value-of select="."/>
                <xsl:choose>
                    <xsl:when test="@path">
                        <span style="margin-right: 10px;"></span>
                        <a href="{@path}" target="_blank">
                            <i class="fa fa-download"></i> 
                        </a>
                    </xsl:when>
                </xsl:choose>
            </span>
        </li>
    </xsl:template>
    
</xsl:stylesheet>