var http = require('http')
var url = require('url')
var fs = require('fs')
var pug = require('pug') 

var estilo = /w3\.css/
var index = /^\/index$/
var musicas = /^\/musicas\/m/

http.createServer((req, res)=>{
    var purl = url.parse(req.url, true)

    if (index.test(purl.pathname)) {
        res.writeHead(200, {'Content-Type':'text/html'})
        fs.readFile('json/index.json', (erro, dados)=>{
            if (!erro) {
                var myObj = JSON.parse(dados); /* Transforma XML > JSON/STRING > Objecto */
                res.write(pug.renderFile('views/index.pug', {ind:myObj}));
            } else {
                res.write('<p><b> ERRO: </b> ' + erro + '</p>')
            }
            res.end()
        })
    } else if (estilo.test(purl.pathname)){
        res.writeHead(200, {'Content-Type':'text/css'})
        fs.readFile('style/w3.css', (erro, dados)=>{
            if (!erro) {
                res.write(dados);
            } else {
                res.write('<p><b> ERRO: </b> ' + erro + '</p>')
            }
            res.end()
        })
    } else if (musicas.test(purl.pathname)) {
        var file = 'json/' + purl.pathname.split('/')[2] + '.json'
        res.writeHead(200, {'Content-Type':'text/html'})
        var myObjIdx
        fs.readFile('json/index.json', (erro, dados)=>{
            if (!erro) {
                    myObjIdx = JSON.parse(dados);
                    fs.readFile(file, (erro, dados)=>{
                        if (!erro) {
                            var myObj = JSON.parse(dados); /* Transforma XML > JSON/STRING > Objecto */
                            res.write(pug.renderFile('views/musicas.pug', {mus:myObj, ind:myObjIdx}));
                        } else {
                            res.write('<p><b> ERRO: </b> ' + erro + '</p>')
                        }
                        res.end()
                    })
            } else {
                res.write('<p><b> ERRO: </b> ' + erro + '</p>')
                res.end()
            }

        })

    } else {
        res.writeHead(200, {'Content-Type':'text/html'})
        res.write('<p>Page not found.</p>')
        res.end();
    }
}).listen(3000, ()=>{
    console.log('Servidor à escuta na porta 3000...')
});
