var express = require('express')
var http = require('http')
var logger = require('morgan')
var fs = require('fs')
var pug = require('pug')
var formidable = require('formidable')
var jsonFile = require('jsonfile')

var app = express() 

app.use(logger('tiny'))
app.use(express.static('stylesheets'))
app.use('/uploaded', express.static('uploaded'))

app.all('*', (req, res, next)=>{
    if(req.url != '/w3.css') {
        res.writeHead(200, {'Content-Type' : 'text/html; charset=utf-8'})
    } else {
        res.writeHead(200, {'Content-Type' : 'text/css'})
    }
    next()
})

/* Landing page */
app.get('/', (req, res)=> {
    jsonFile.readFile('data/ficheiros.json', (erro, dados)=>{
        if (!erro) {
            res.write(pug.renderFile('views/index.pug', {ficheiros: dados}))
            res.end()
        }
    })

})

app.get('*', (req,res)=>{
    res.end('Nao tratado')
})

/* Processa o formulário */
app.post('/processForm', (req, res)=>{
    var form = new formidable.IncomingForm()
    form.parse(req, (err, fields, files)=>{
        var fenviado = files.ficheiro.path
        var fnovo = './uploaded/' + files.ficheiro.name
        console.dir(files)

        fs.rename(fenviado, fnovo, (erro)=>{
            if(!erro) {
                jsonFile.readFile('data/ficheiros.json', (erro, dados)=>{
                    if(!erro) {
                        var result = Object.assign({},fields, 
                            {"nome" : files.ficheiro.name, 
                             "path" : fnovo, 
                             "tamanho" : files.ficheiro.size / 1000000 ,
                             "tipo" : files.ficheiro.type
                            });
                        dados.push(result)
                        jsonFile.writeFile('data/ficheiros.json', dados, (erro)=>{
                            res.write(pug.renderFile('views/index.pug', {ficheiros: dados}))
                            res.end()
                        })
                    }
                }) 

            } else {
                res.write(pug.renderFile('views/erro.pug', {e: 'Ocorreu um erro.'}))
                res.end()
            }
        })
    
    }) 
})

http.createServer(app).listen(4007)