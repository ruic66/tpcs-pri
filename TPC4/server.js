var http = require('http');
var url = require('url');
var fs = require('fs');

http.createServer((req, res) => {

	var parsed_url = url.parse (req.url, true);

	var query_info = parsed_url.query;

	var filepath;

	if (parsed_url.pathname === "/obras") filepath = "website/index.html"
	else if (parsed_url.pathname === "/obra") 
			if (query_info.id) filepath = "website/html/obra" + query_info.id + ".html"

	if (filepath) {
		fs.readFile(filepath, (err, data) => {
			res.writeHead(200, {'Content-Type': 'text/html'});
			if(!err){
				res.write(data);
			} else {
				res.write(err);
			}
			res.end();
		});
	}
	else {
		res.end();
	}
}).listen(4001, ()=> {
	console.log("Servidor à escuta na porta 4001.");
});