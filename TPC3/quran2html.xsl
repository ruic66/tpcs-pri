<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="2.0">
    
    <xsl:output method="html" indent="yes"/>
    
    <xsl:template match="/">
        <xsl:result-document href="website/index.html">
            <html>
                <head>
                    <meta charset="UTF-8" />
                    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"/>
                </head>
                <body>
                    <h1 class="w3-serif" style="text-align: center;">
                        <i>
                            <xsl:value-of select="tstmt/coverpg/title"/>
                        </i>
                    </h1>
                    <h3 class="w3-serif" style="text-align: center;">
                        <i>
                            <xsl:value-of select="tstmt/coverpg/title2"/>
                        </i>
                    </h3>
                    
                    <hr/>
                   
                    
                    <div class="w3-panel">
                        <b class="w3-serif"><xsl:value-of select="tstmt/preface/ptitle"/></b>
                        <p class="w3-serif"><xsl:value-of select="tstmt/preface/p"/></p>
                    </div>
                    
                    <hr/>
                    <div class="w3-panel">
                        <b class="w3-serif">Index</b>
                        
                        <ul class="w3-ul w3-hoverable" style="column-count: 3; list-style: none;">
                            <xsl:apply-templates select="//sura" mode="indice"/>
                            
                        </ul>
                    </div>
    
                    
                    <hr/>
                    
                    <div class="w3-panel w3-serif">
                        <b>More information</b>
                        <xsl:for-each select="tstmt/coverpg/subtitle/p">
                            <p>
                                <xsl:value-of select="."/>
                            </p>
                        </xsl:for-each>
                    </div>
                </body>
            </html>
        </xsl:result-document>
        
        <xsl:apply-templates/>
            
        
    </xsl:template>
    
    <xsl:template match="sura" mode="indice">
        <a href="sura{count(preceding-sibling::*) + 1}.html" style="text-decoration: none;">
            <li class="w3-serif">
                <i>
                    <xsl:value-of select="bktlong"/>
                </i>
            </li>
        </a>
    </xsl:template>
    
    <xsl:template match="sura">
        <xsl:result-document href="website/sura{count(preceding-sibling::*) + 1}.html">
            <html>
                <head>
                    <meta charset="UTF-8" />
                    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
                    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"/>
                </head>
                <body>
                    <h1 style="text-align: center; font-family: serif;"><i><xsl:value-of select="bktlong"/></i></h1>
                    <xsl:choose>
                        <xsl:when test="epigraph">
                            <div class="w3-panel w3-leftbar">
                                <p><i class="fa fa-quote-right w3-medium"></i><br/>
                                    <i class="w3-serif w3-xlarge"><xsl:value-of select="epigraph"/></i></p>
                            </div>
                        </xsl:when>
                    </xsl:choose>
                    <hr width="70%"/>
                    <xsl:apply-templates/>
                    <div style="text-align: center;">
                        <a style="text-decoration: none;" href="sura{count(preceding-sibling::*)}.html"><i class="fa fa-lg fa-arrow-left"></i></a>
                       <span style="padding-right: 2em"></span> 
                        <a style="text-decoration: none;" href="index.html"><i class="fa fa-lg fa-home"></i></a>
                       <span style="padding-right: 2em"></span> 
                        <a style="text-decoration: none;" href="sura{count(preceding-sibling::*) + 2}.html"><i class="fa fa-lg fa-arrow-right"></i></a>
                    </div>
                </body>
            </html>
        </xsl:result-document>
    </xsl:template>
    
    <xsl:template match="text()" priority="-1"/>
    
    <xsl:template match="v">
        <div class="w3-panel w3-leftbar">
            <p><b><i class="w3-serif w3-medium"><xsl:value-of select="count(../preceding-sibling::sura) + 1"/>:<xsl:value-of select="count(preceding-sibling::v) + 1"/></i></b><br/>
                <i class="w3-serif w3-medium"><xsl:value-of select="."/></i></p>
        </div>
    </xsl:template>
    
</xsl:stylesheet>