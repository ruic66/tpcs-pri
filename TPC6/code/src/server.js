var http = require('http')
var url = require('url')
var pug = require('pug')
var fs = require('fs')
var {parse} = require('querystring')
var jsonfile = require('jsonfile')

const myBD = "data/teses.json"

// Cria o servidor

var myServer = http.createServer((req, res)=>{
    var purl = url.parse(req.url, true)
    var query = purl.query
    
    console.log("Pedido: " + req.url)
    console.log("Método: " + req.method)
    console.log("<<<<<<<>>>>>>>")

    // método GET
    if (req.method == 'GET') {
        // Página inicial
        if (purl.pathname == "/") {
            res.writeHead(200, {"Content-Type": "text/html;charset=utf-8"})
            res.write(pug.renderFile("views/index.pug")) 
            res.end()
        }
        // Página de registar
        else if (purl.pathname == "/registo") {
            res.writeHead(200, {"Content-Type": "text/html;charset=utf-8"})
            res.write(pug.renderFile("views/form-tese.pug")) 
            res.end()
        } 
        // Página de listagem
        else if (purl.pathname == "/lista") {
            jsonfile.readFile(myBD, (erro, teses)=>{
                if(!erro) {
                    res.writeHead(200, {"Content-Type": "text/html;charset=utf-8"})
                    res.write(pug.renderFile("views/lista.pug", {teses: teses})) 
                    res.end()
                } else {

                }
            })
        } 
        // Folha de estilo
        else if (purl.pathname == "/w3.css") {
            res.writeHead(200, {"Content-Type": "text/css"})
            fs.readFile("style/w3.css", (erro, dados)=>{
                if (!erro) {
                    res.write(dados);
                } else {
                    res.write(pug.renderFile("erro.pug", {e: erro})) 
                }
                res.end()
            })
            // Rotas não implementadas
        } else {
            res.writeHead(501, {"Content-Type": "text/html;charset=utf-8"}) 
            res.end("Erro: " + purl.pathname + " não se encontra implementado.")
        }
    // método POST
    } else if (req.method == 'POST') {
        if (purl.pathname == "/processaForm") {
            processaBody(req, (result)=>{
                jsonfile.readFile(myBD, (erro, teses)=>{
                    if (!erro) {
                        teses.push(result)
                        jsonfile.writeFile(myBD, teses, (erro2)=>{
                            if(!erro2) {
                                res.writeHead(200, {"Content-Type": "text/html;charset=utf-8"}) 
                                res.end(pug.renderFile("views/success.pug"));
                            } else {
                                console.log(erro2)
                            }
                        })
                    } else {
                        console.log(erro)
                    }
                })
            })
        } else {
            res.writeHead(501, {"Content-Type": "text/html;charset=utf-8"}) 
            res.end("Erro: " + purl.pathname + " não se encontra implementado.")
        }
    } else {
        res.writeHead(501, {"Content-Type": "text/css;charset=utf-8"}) 
        res.end("Método: " + req.method + " não suportado.")
    }

})

// Coloca servidor à escuta na porta 4006

myServer.listen(4006, ()=>{
    console.log("Servidor à escuta na porta 4006....")
})

// Função processaBody 

function processaBody(request, callback) {
    const FORM_URLENCODED = "application/x-www-form-urlencoded"

    if (request.headers['content-type'] === FORM_URLENCODED) {
        var body = ""
        request.on('data', (chunk)=>{
            body += chunk.toString()
        })
        request.on('end', ()=>{
            callback(parse(body))
        })
    } else {
        callback(null);
    }
}